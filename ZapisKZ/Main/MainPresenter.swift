//
//  MainPresenter.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import Foundation

protocol MainPresenterProtocol {
    init(interactor: MainInteractorProtocol, view: MainViewProtocol, wireframe: MainWireframeProtocol)
    func viewDidLoad()
    func navigateToDetails(with id: Int)
}
class MainPresenter: MainPresenterProtocol {
    internal var wireframe: MainWireframeProtocol!
    internal var interactor: MainInteractorProtocol
    internal weak var view: MainViewProtocol!
    required init(interactor: MainInteractorProtocol, view: MainViewProtocol, wireframe: MainWireframeProtocol){
        self.interactor = interactor
        self.view = view
        self.wireframe = wireframe
    }
    func viewDidLoad() {
        interactor.executeSingle { [weak self] (result) in
            guard let strongSelf = self else {return}
            switch result {
            case .success(let cells):
                strongSelf.view.viewDidReceiveCells(cells: cells)
            case .failure(let error):
                strongSelf.view.viewDidReceiveErrorMessage(errorMessage: error.localizedDescription)
            }
        }
    }
    func navigateToDetails(with id: Int) {
        wireframe.navigateToDetails(with: id)
    }
}
