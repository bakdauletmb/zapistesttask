//
//  MasterCollectionViewCell.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import UIKit

class MasterCollectionViewCell: UICollectionViewCell {
    var titleLabel: UILabel = {
        var label = UILabel()
            label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    var bannerImageView: UIImageView = {
        var view = UIImageView()
            view.layer.cornerRadius = 20
            view.clipsToBounds = true
        return view
    }()
    //Наверняка вы юзали  custom layout и еще один collectionView, но я воспользуюсь  полумерами
    var secondBannerImageView: UIImageView = {
        var view = UIImageView()
            view.layer.cornerRadius = 10
            view.clipsToBounds = true
        return view
    }()
    var thirdBannerImageView: UIImageView = {
        var view = UIImageView()
            view.layer.cornerRadius = 10
            view.clipsToBounds = true
        return view
    }()
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError()
    }
    func setupView(){
        addSubview(bannerImageView)
        bannerImageView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview()
            make.width.equalTo(150)
            make.height.equalTo(150)
        }
        addSubview(secondBannerImageView)
        secondBannerImageView.snp.makeConstraints { (make) in
            make.top.equalTo(bannerImageView)
            make.left.equalTo(bannerImageView.snp.right).offset(4)
            make.height.equalTo(70)
            make.right.equalTo(-2)
        }
        addSubview(thirdBannerImageView)
        thirdBannerImageView.snp.makeConstraints { (make) in
            make.top.equalTo(secondBannerImageView.snp.bottom).offset(2)
            make.left.equalTo(bannerImageView.snp.right).offset(4)
            make.right.equalTo(-2)
            make.bottom.equalTo(bannerImageView)
        }
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(bannerImageView.snp.bottom).offset(2)
            make.left.equalTo(bannerImageView)
            make.right.lessThanOrEqualTo(0)
            
        }
    }
    func set(model: MasterProtocol){
        bannerImageView.sd_setImage(with: URL(string: NetworkManager.baseImageURL + model.pictures![0]))
        titleLabel.text = model.name
        secondBannerImageView.sd_setImage(with: URL(string: NetworkManager.baseImageURL + model.pictures![1]))
        thirdBannerImageView.sd_setImage(with: URL(string: NetworkManager.baseImageURL + model.pictures![2]))
    }
}
