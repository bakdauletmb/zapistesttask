//
//  MasterTableViewCell.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import UIKit

class MasterTableViewCell: UITableViewCell{
    var delegate : CellDelegate?
    var bannerLabel: UILabel = {
        var banner = UILabel()
            banner.font = .boldSystemFont(ofSize: 16)
        return banner
    }()
    var collectionView : UICollectionView = {
        var layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
        var collectionView = UICollectionView(frame: .zero, collectionViewLayout:  layout)
        collectionView.backgroundColor = .white
        
        return collectionView
    }()
    var model: MasterViewModel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
        setupCollectionView()
        selectionStyle = .none
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(model: MasterViewModel){
        self.bannerLabel.text = model.title
        self.model = model
    }
    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MasterCollectionViewCell.self, forCellWithReuseIdentifier: "MasterCollectionViewCell")
    }
    func setupConstraints(){
        contentView.addSubview(bannerLabel)
        bannerLabel.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.left.equalToSuperview()
        }
        contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.height.equalTo(250)
            make.top.equalTo(bannerLabel.snp.bottom).offset(4)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(10)
        }
    }
}
extension MasterTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model.content.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MasterCollectionViewCell", for: indexPath) as!  MasterCollectionViewCell
            cell.set(model: model.content[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(at: model.content[indexPath.item].id)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 200)
    }
}
