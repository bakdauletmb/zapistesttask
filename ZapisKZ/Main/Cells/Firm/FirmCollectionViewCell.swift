//
//  FirmCollectionViewCell.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import UIKit
import SDWebImage
class FirmCollectionViewCell: UICollectionViewCell {
    var titleLabel: UILabel = {
        var label = UILabel()
            label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    var bannerImageView: UIImageView = {
        var view = UIImageView()
            view.layer.cornerRadius = 20
            view.clipsToBounds = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError()
    }
    func setupView(){
        addSubview(bannerImageView)
        bannerImageView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(150)
        }
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(bannerImageView.snp.bottom).offset(4)
            make.left.equalTo(bannerImageView)
            make.right.lessThanOrEqualTo(0)
            
        }
    }
    func set(model: FirmProtocol){
        bannerImageView.sd_setImage(with: URL(string: NetworkManager.baseImageURL + model.pictureUrl))
        titleLabel.text = model.name
        
    }
}
