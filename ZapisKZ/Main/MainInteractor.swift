//
//  MainInteractor.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import Foundation

protocol MainInteractorProtocol {
    init(worker: MainWorkerProtocol)
    func executeSingle(closure: @escaping (Result<[CellType],Error>)->())
}

final class MainInteractor: MainInteractorProtocol {
    private var worker: MainWorkerProtocol!
    required init(worker: MainWorkerProtocol) {
        self.worker  = worker
    }
    func executeSingle(closure: @escaping (Result<[CellType],Error>)->()) {
        worker.makeRequest { (result) in
            switch result {
            case .success(let response):
                closure(.success(self.buildCells(input: response.data)))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    private func buildCells(input: ResponseDataModel) -> [CellType]{
        var cells = [CellType]()
            cells.reserveCapacity(4)
        cells.append(.firm(model: FirmViewModel(title: "Недавно добавлены", content: input.recentlyAddedFirms)))
        cells.append(.firm(model: FirmViewModel(title: "Популярные", content: input.popularFirms)))
        cells.append(.firm(model: FirmViewModel(title: "Рекомендуем", content: input.recommendedFirms)))
        cells.append(.master(model: MasterViewModel(title: "Мастера", content: input.masters)))
        
        return cells
    }
}
