//
//  MainView.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import UIKit
import SnapKit

//Так как большинство тестовых заданий уходят в небытие, не будучи никем проверены, я зарекся тратить на них больше пары часов, поэтому сэкономил время на View в VIPER

protocol MainViewProtocol: class {
    func viewDidReceiveCells(cells: [CellType])
    func viewDidReceiveErrorMessage(errorMessage: String)
}

final class MainViewController: UIViewController {
    public var presenter: MainPresenterProtocol?
    private var cells: [CellType] = []
    private var tableView: UITableView = {
        var tableView = UITableView()
        
        return tableView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter?.viewDidLoad()
        setupConstraints()
        setupTableView()
    }
    private func setupTableView(){
        tableView.dataSource = self
        tableView.register(FirmTableViewCell.self, forCellReuseIdentifier: "FirmTableViewCell")
        tableView.register(MasterTableViewCell.self, forCellReuseIdentifier: "MasterTableViewCell")
        tableView.separatorStyle = .none
    }
    private func setupConstraints(){
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension MainViewController: MainViewProtocol{
    func viewDidReceiveCells(cells: [CellType]) {
        self.cells = cells
        self.tableView.reloadData()
    }
    func viewDidReceiveErrorMessage(errorMessage: String){
        //Show alert with error
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cells[indexPath.row] {
        case .firm(let model):
        let cell = tableView.dequeueReusableCell(withIdentifier: "FirmTableViewCell", for: indexPath) as! FirmTableViewCell
            cell.delegate = self
            cell.set(model: model)
            return cell
        case .master(let model):
        let cell = tableView.dequeueReusableCell(withIdentifier: "MasterTableViewCell", for: indexPath) as! MasterTableViewCell
            cell.delegate = self
            cell.set(model: model)
        return cell
    }
    }
}
extension MainViewController: CellDelegate{
    func didSelect(at id: Int) {
        presenter?.navigateToDetails(with: id)
    }
}
