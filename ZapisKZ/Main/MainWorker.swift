//
//  MainWorker.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import Foundation
import Alamofire

protocol MainWorkerProtocol {
    func makeRequest(closure: @escaping (Result<ResponseModel<ResponseDataModel>,Error>)->())
}
class MainWorker: MainWorkerProtocol {
    func makeRequest(closure: @escaping (Result<ResponseModel<ResponseDataModel>, Error>) -> ()) {
        AF.request(NetworkManager.mainURL, method: .get).responseData { data in
            switch data.result {
            case .success(let data):
                do {
                    let response = try JSONDecoder().decode(ResponseModel<ResponseDataModel>.self, from: data)
                    closure(.success(response))
                }
                catch {
                    closure(.failure(error))
                }
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
}
