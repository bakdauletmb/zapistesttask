//
//  MainWireframe.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import UIKit
protocol MainWireframeProtocol {
    func buildMainModule() -> UIViewController
    func navigateToDetails(with id: Int)
}
final class MainWireframe: MainWireframeProtocol {
    private var viewController: UINavigationController!
    func buildMainModule() -> UIViewController {
        let tempViewController = MainViewController()
        let worker : MainWorkerProtocol = MainWorker()
        let interactor: MainInteractorProtocol = MainInteractor(worker: worker)
        tempViewController.presenter = MainPresenter(interactor: interactor, view: tempViewController, wireframe: self)
        self.viewController = UINavigationController(rootViewController: tempViewController)
    
        return self.viewController
    }
    func navigateToDetails(with id: Int) {
        let wireframe: DetailsWireframeProtocol = DetailsWireframe()
        let vc = wireframe.buildDetailsModule(with: id)
        self.viewController.pushViewController(vc, animated: true)
    }
}
