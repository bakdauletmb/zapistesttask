//
//  MainEntity.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import Foundation

struct ResponseModel<T: Decodable>: Decodable {
    var message: String
    var data: T
}
struct ResponseDataModel: Decodable {
    var recommendedFirms: [Firms]
    var popularFirms: [Firms]
    var recentlyAddedFirms: [Firms]
    var masters: [Firms]
    var isCategoriesHidden: Bool
    var cities: [Cities]
    var wrongServicePricePhones: [String]
}

struct Firms: MasterProtocol, Decodable {
    var id: Int
    var name: String
    var address: String
    var type: String
    var checkRating: Int
    var urlKey: String
    var isPromoted: Bool
    var avatarUrl: String
    var isIndividualMaster: Bool
    var workSchedule: String
    var pictureUrl: String
    var averageRating: Double
    var todayReservationsCount: String?
    var isFavorite: Bool
    var pictures: [String]?
}
struct Cities: Decodable {
    var id: Int
    var name: String
    var urlName: String
    var longitude: Double
    var latitude: Double
}

enum CellType {
    case firm(model: FirmViewModel)
    case master(model: MasterViewModel)
}

struct FirmViewModel {
    var title: String
    var content: [FirmProtocol]
}

protocol FirmProtocol {
    var id: Int {get set}
    var name: String {get set}
    var address: String {get set}
    var type: String {get set}
    var checkRating: Int {get set}
    var isPromoted: Bool {get set}
    var avatarUrl: String {get set}
    var isIndividualMaster: Bool {get set}
    var workSchedule: String {get set}
    var pictureUrl: String {get set}
    var averageRating: Double {get set}
    var todayReservationsCount: String? {get set}
    var isFavorite: Bool {get set}
}
protocol MasterProtocol: FirmProtocol {
    var pictures: [String]? {get set}
}

struct MasterViewModel{
    var title: String
    var content: [MasterProtocol]
}
