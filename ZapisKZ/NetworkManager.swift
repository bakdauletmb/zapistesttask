//
//  NetworkManager.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/30/21.
//

import Foundation

class NetworkManager {
    static var mainURL = "http://zs.khangroup.kz/rest/clients-app/v1/screen/home"
    static var detailsURL = "http://zs.khangroup.kz/rest/clients-app/v1/firms/"
    static var baseImageURL = "http://zs.khangroup.kz"
}
