//
//  MapViewController.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import UIKit
import YandexMapsMobile

final class MapViewController: UIViewController {
    private var mapView = YMKMapView()
    private var location: Location
    
    init(location: Location) {
        self.location = location
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupMap()
    }
    
    private func setupMap(){
        mapView.mapWindow.map.move(
            with: YMKCameraPosition(target: YMKPoint(latitude: location.centerY, longitude: location.centerX), zoom: Float(location.zoom), azimuth: 0, tilt: 0),
             animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 2),
             cameraCallback: nil)
    }
    private func setupView(){
        view.addSubview(mapView)
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

