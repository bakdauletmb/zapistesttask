//
//  DetailsEntity.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import Foundation

struct DetailsModel: Decodable {
    var location: Location
}
struct MastersModel: Decodable{
    var id: Int
    var name: String
    var surname: String
}


struct Location: Decodable {
    var markerX: Double
    var markerY: Double
    var centerX: Double
    var centerY: Double
    var zoom:    Double
}
