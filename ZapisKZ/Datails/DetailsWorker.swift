//
//  DetailsWorker.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import Foundation
import Alamofire
protocol DetailsWorkerProtocol{
    func fetchData(id: Int, completion: @escaping (Result<DetailsModel,Error>)->())
}

final class DetailsWorker: DetailsWorkerProtocol{
    func fetchData(id: Int, completion: @escaping (Result<DetailsModel,Error>)->()) {
        AF.request(NetworkManager.detailsURL + "\(id)").responseData { (data) in
            switch data.result {
            case .success(let data):
                do {
                    let decoded = try JSONDecoder().decode(ResponseModel<DetailsModel>.self, from: data)
                    completion(.success(decoded.data))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
