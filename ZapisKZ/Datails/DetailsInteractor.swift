//
//  DetailsInteractor.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import Foundation

protocol DetailsInteractorProtocol {
    var presenter: DetailsPresenterProtocol? {get set}
    var worker: DetailsWorkerProtocol? {get set}
    func fetchData(withID id: Int)
    
}
final class DetailsInteractor: DetailsInteractorProtocol {
    internal var worker: DetailsWorkerProtocol?
    internal var presenter: DetailsPresenterProtocol?
    
    func fetchData(withID id: Int) {
        worker?.fetchData(id: id, completion: { [weak self] (result) in
            switch result{
            case .failure(let error):
                self?.presenter?.presenterDidReceiveError(error: error.localizedDescription)
            case .success(let model):
                self?.presenter?.presenterDidReceiveData(data: model)
            }
        })
    }
}
