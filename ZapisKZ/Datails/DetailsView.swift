//
//  DetailsView.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import UIKit
protocol DetailsViewProtocol: class {
    func showError(error: String)
    func showSuccess()
    func receiveData(data: DetailsModel)
}

final class DetailsViewController: UIViewController {
    public var presenter: DetailsPresenterProtocol?
    private var mapButton: UIButton = {
        var button = UIButton()
            button.setTitle("Navigate to the map", for: .normal)
            button.setTitleColor(.black, for: .normal)
        
        return button
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter?.viewDidLoad()
        setupView()
        addTarget()
    }
    private func setupView(){
    //     Верстать нормально тысячу лет ребят(
        view.addSubview(mapButton)
        mapButton.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.height.equalTo(100)
            $0.width.equalTo(200)
        }
    }
    func addTarget(){
        mapButton.addTarget(self, action: #selector(navigationButtonPressed), for: .touchUpInside)
    }
    @objc func navigationButtonPressed(){
        presenter?.navigateToTheMap()
    }
}
extension DetailsViewController: DetailsViewProtocol{
    func showError(error: String) {
        //Alert with error
    }
    
    func showSuccess() {
        //Success alert
    }
    
    func receiveData(data: DetailsModel) {
        //set images etc.
    }
}

