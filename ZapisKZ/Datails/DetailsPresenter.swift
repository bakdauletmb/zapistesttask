//
//  DetailsPresenter.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import Foundation

protocol DetailsPresenterProtocol {
    init(interactor: DetailsInteractorProtocol,view: DetailsViewProtocol,wireframe: DetailsWireframeProtocol,id: Int)
    func viewDidLoad()
    func presenterDidReceiveError(error: String)
    func presenterDidReceiveData(data: DetailsModel)
    func navigateToTheMap()
}
final class DetailsPresenter: DetailsPresenterProtocol {
    private weak var view: DetailsViewProtocol?
    private var wireframe: DetailsWireframeProtocol
    private var interactor: DetailsInteractorProtocol
    private var id: Int
    private var location: Location?
    func presenterDidReceiveError(error: String) {
        view?.showError(error: error)
    }
    
    func presenterDidReceiveData(data: DetailsModel) {
        view?.showSuccess()
        view?.receiveData(data: data)
        self.location = data.location
    }
    
    required init(interactor: DetailsInteractorProtocol,view: DetailsViewProtocol,wireframe: DetailsWireframeProtocol,id: Int) {
        self.view = view
        self.wireframe = wireframe
        self.interactor = interactor
        self.id = id
    }
    
    func viewDidLoad() {
        interactor.fetchData(withID: id)
    }
    func navigateToTheMap() {
        guard location != nil else {
            view?.showError(error: "Location is not loaded")
            return
        }
        wireframe.navigateToMap(with: location!)
    }
}
