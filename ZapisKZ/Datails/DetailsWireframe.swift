//
//  DetailsWireframe.swift
//  ZapisKZ
//
//  Created by Bakdaulet on 1/31/21.
//

import UIKit
protocol DetailsWireframeProtocol {
    func buildDetailsModule(with id: Int) -> UIViewController
    func navigateToMap(with location: Location)
}

final class DetailsWireframe: DetailsWireframeProtocol {
    private var viewController: UIViewController!
    func buildDetailsModule(with id: Int) -> UIViewController {
        let vc = DetailsViewController()
        var interactor: DetailsInteractorProtocol = DetailsInteractor()
        let presenter: DetailsPresenterProtocol = DetailsPresenter(interactor: interactor, view: vc, wireframe: self, id: id)
        let worker: DetailsWorkerProtocol = DetailsWorker()
        vc.presenter = presenter
        interactor.presenter = presenter
        interactor.worker = worker
        self.viewController = vc
        return self.viewController
    }
    func navigateToMap(with location: Location){
        viewController.navigationController?.pushViewController(MapViewController(location: location), animated: true)
    }
}
